from os import scandir, getcwd
from ebook import EBook

class EReader:
  """Representa un lector de libros eléctronicos"""

  def __init__(self):
    self.lista_libros = self.get_directorio("libros/")

  def get_libros(self, filtro):
    """Retorna un list() que contiene los libros según el 'filtro' indicado.

    'filtro' es una string que puede ser el titulo o autor que coincida con uno o varios libros. Cualquier otro valor recibido deberá lanzar una excepción de tipo ValueError."""
    libros_encontrados = []
    for libro in self.lista_libros:
      if libro.titulo == filtro.strip() or libro.autor == filtro.strip():
        #print(libro.titulo)
        #print(libro.autor)
        libros_encontrados.append(libro)

    if len(libros_encontrados) == 0:
      raise ValueError("No se encuentran libros que coincidan con el parametro de filtrado especificado.") 

    return libros_encontrados

  def get_directorio(self, directorio):
    lista_libros = []
    
    for fichero in scandir(directorio):
      ruta = directorio+fichero.name
      autor = "sin autor"
      titulo = "sin titulo"
      contenido = ""

      obj = open(ruta,"r")      
      for linea in obj.readlines():
        if linea.find("autor:") >= 0:
          linea_autor = linea.split(':')
          autor = linea_autor[linea.find("autor") + 1].replace("\n","").strip()
        elif linea.find("titulo:") >= 0:
          linea_titulo = linea.split(':')
          titulo = linea_titulo[linea.find("titulo") + 1].replace("\n","").strip()
        else:
          contenido += linea
      
      lista_libros.append(EBook(titulo, autor, contenido))

    obj.close()
    return lista_libros      

      
  
