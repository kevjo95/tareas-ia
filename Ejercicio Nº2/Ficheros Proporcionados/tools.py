import os, os.path
from os import scandir, getcwd
from email import EMail
import numpy as np

class Tools:

  def __init__(self):
    self.lista_emails = self.get_emails(["emails/test/","emails/train/"])
    self.lista_emails_train = self.get_emails(["emails/train/"])
    self.lista_emails_test = self.get_emails(["emails/test/"])
    self.vector_palabras = self.get_vector_palabras()
    self.cantidad_emails_train = self.get_cant_archivos("emails/train/")
    self.cantidad_emails_test = self.get_cant_archivos("emails/test/")
    
  def get_cant_archivos(self, directorio):
    return len([name for name in os.listdir(directorio) if os.path.isfile(os.path.join(directorio, name))])

  def get_vector_palabras(self):
    lista_palabras = []
    #lista_emails = self.get_emails(["emails/test/","emails/train/"])
    for email in self.lista_emails:
      lista_palabras += email.contenido.split(' ')  
    lista_palabras = list(set(lista_palabras))
    lista_palabras.sort()
    lista_palabras.remove("")
    return lista_palabras

  def get_emails(self, directorios):
    lista_emails = []
      
    for directorio in directorios: 
      for fichero in scandir(directorio):
        ruta = directorio+fichero.name
        es_spam = -1
        contenido = ""
        num_linea = 0
        obj = open(ruta,"r")      
        for linea in obj.readlines():
          num_linea += 1
          if num_linea == 1:
              es_spam = linea
          else:
            contenido += linea

        lista_emails.append(EMail(fichero.name, es_spam, contenido))
    obj.close()
    return lista_emails   

  def inicializar_array_zeros(self, tipo):
    if tipo == "train_set_x":
      narray = np.zeros((len(self.vector_palabras), self.cantidad_emails_train))
    elif tipo == "train_set_y":
      narray = np.zeros((1, self.cantidad_emails_train))
    elif tipo == "test_set_x":
      narray = np.zeros((len(self.vector_palabras), self.cantidad_emails_test))
    elif tipo == "test_set_y":
      narray = np.zeros((1, self.cantidad_emails_test))

    return narray
  
  def load_dataset(self):
    train_set_x = self.inicializar_array_zeros("train_set_x")
    train_set_y = self.inicializar_array_zeros("train_set_y")
    test_set_x = self.inicializar_array_zeros("test_set_x")
    test_set_y = self.inicializar_array_zeros("test_set_y")

    i, j = -1, -1
    for email in self.lista_emails_train:
      j = -1
      i += 1
      train_set_y[0,i] = email.es_spam
      for palabra in self.vector_palabras:
        j += 1
        train_set_x[j,i] = email.lista_palabras.count(palabra)
        
    i, j = -1, -1
    for email in self.lista_emails_test:
      j = -1
      i += 1
      test_set_y[0,i] = email.es_spam
      for palabra in self.vector_palabras:
        j += 1
        test_set_x[j,i] = email.lista_palabras.count(palabra)

    return (train_set_x, test_set_x, train_set_y, test_set_y)
