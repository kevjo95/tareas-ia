from tools import Tools
import rl_functions as rl

class SpamDetector:

  def __init__(self):
    """El método init leerá todos los archivos con extensión ".txt" de los directorios emails/train y emails/test y los usará para crear las matrices de X_train, X_test, Y_train y Y_test.

    Cada archivo ".txt" corresponderá a una instancia de entrenamiento o prueba. El formato de los archivos ".txt" es el siguiente:

    <0|1>
    <contenido>

    La primera línea contendrá la etiqueta 0 si el contenido no es spam y 1 si lo es. A partir de la segunda línea todo el texto que se encuentre se considerará parte del correo electrónico.

    El texto de un correo electrónico se convertirá en un vector de atributos de la siguiente forma:
    Primero se detectarán todas las palabras distintas (sólo letras, ningún símbolo) que aparezcan todos los archivos (train + test), luego a cada palabra se le asignará un índice y finalmente se representará cada correo a través de un vector cuyos elementos reprsentarán la cantidad de veces que aparece cada palabra en su contenido de acuerdo al índice que se asignó.

    Ejemplo:

    Si tuviésemos solamente tres palabras distintas: no, si, spam, le asignaríamos a cada un índice de 0 al 2 (esta longitud representa a n). Y si el contenido de un correo fuese: "spam spam si si" el vector que lo representaría sería: 
    [0 2 2]

    Recuerde que todos los vectores deben tener la misma longitud. 

    Las URL se considerarán como palabras, ej.: http://www.google.com sería cuatro palabras: http, www, google y com.

    Por simplicidad se manejarán solamente correos en español. 
    """
    tls = Tools()
    self.train_set_x, self.test_set_x, self.train_set_y, self.test_set_y = tls.load_dataset()

  def train_model(self):
    """Retorna un un diccionario con los siguientes datos:
    
    d = {"costs": costs,
    "Y_prediction_test": Y_prediction_test, 
    "Y_prediction_train" : Y_prediction_train, 
    "w" : w, 
    "b" : b,
    "learning_rate" : learning_rate,
    "num_iterations": num_iterations}

    Los parámetros learning_rate y num_iterations deben ser definidos por el programador mediante un proceso de prueba y error con el fin de obtener la mejor precisión en los datos de prueba.
    """
    d = rl.model(self.train_set_x, self.train_set_y, self.test_set_x, self.test_set_y, learning_rate = 0.09, num_iterations = 2000, print_cost = True)
    return d

  def get_datasets(self):
    """Retorna un diccionario con los datasets preprocesados con los datos y 
    dimensiones que se usaron para el entrenamiento
     
    d = { "X_train": X_train,
    "X_test": X_test,
    "Y_train": Y_train,
    "Y_test": Y_test
    }
    """
    d = {
      "X_train": self.train_set_x,
      "X_test": self.test_set_x, 
      "Y_train": self.train_set_y, 
      "Y_test": self.test_set_y,
    }
    return d