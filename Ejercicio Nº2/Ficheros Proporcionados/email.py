import unicodedata
import re

class EMail:

  def __init__(self, nombre, es_spam, contenido):
    self.nombre = nombre
    self.es_spam = es_spam
    self.contenido = self.filtro_palabras(contenido)
    self.lista_palabras = self.get_lista_palabras()

  def filtro_palabras(self, contenido):
    contenido_limpio = contenido.lower()
    contenido_limpio = contenido_limpio.replace("\n"," ") #Quitamos saltos de linea
    contenido_limpio = self.elimina_tildes(contenido_limpio) #Quitamos los acentos y caracteres similares
    contenido_limpio = re.sub("[^\w]|_"," ", contenido_limpio) #Quitammos caracteres que no sean a-Z 0-9
    
    #param_limpieza = ["?", "¿", "¡", "!", " ", ",", ".", ";", ":","/","(",")","[","]","{","}","@","'","\"","-","_","<",">","*","%","*"]
    #for caracter in param_limpieza:
    #  contenido_limpio = contenido_limpio.replace(caracter, " ")
    return contenido_limpio      

  def elimina_tildes(self, cadena):
    nfkd_form = unicodedata.normalize('NFKD', cadena)
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])

  def get_lista_palabras(self):
    lista_palabras = []
    lista_palabras = self.contenido.split(' ')
    for palabra in lista_palabras:
      if palabra == '':
        lista_palabras.remove(palabra)
    lista_palabras.sort()
    return lista_palabras