# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Por que existe este repositorio? ###
El objetivo de este repositorio es compartir los diversas formas de expresar nuestra creatividad al momento de codificar esos pequeños retos llamados tareas.

### Como puedo leer este repositorio? ###
Si deseas ver como he logrado resolver los problemas, no dudes en hacer tu git clone o navegar entre los ficheros de forma online. 

### Que debo considerar? ###
* El repositorio fue creado para motivos academicos
* Los proyectos son propiedad de su autor y esta completamente prohibido tomarlos como propios sin previo consentimiento.
* Git reporta la fecha y hora en que se realiza un clone, el propietario sabe quien y cuando clona el repositorio.
* El propietario apoya el uso y distribucion de los proyectos de codigo abierto y se debe respetar esto, no esta permitido sacar un provecho que no sea educativo. 

### Quieres resolver tus dudas? ###
Si encuentras una manera de mejorar la solucion por favor no esperes para enviarme un mensaje o correo para que trabajemos juntos. 
kevjooeworld@gmail.com
* Repo owner or admin
